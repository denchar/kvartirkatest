package com.example.kvartirkatestapplication.data.retrofit

import com.example.kvartirkatestapplication.models.CitiesFlatsModel
import com.example.kvartirkatestapplication.models.CityModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("cities?")
    fun getCityByGeoPoint(@Query("lat") lat: String, @Query("lng") lng: String): Observable<CityModel>

    @GET("flats?")
    fun getFlatsByGeoPoint(
        @Query("city_id") city_id: Int,
        @Query("point_lat") lat: Double,
        @Query("point_lng") lng: Double,
        @Query("device_screen_width") screenWidth: Int,
        @Query("device_screen_height") screenHeight: Int
    ): Observable<CitiesFlatsModel>

}