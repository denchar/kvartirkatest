package com.example.kvartirkatestapplication.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.kvartirkatestapplication.Values
import com.example.kvartirkatestapplication.data.retrofit.ApiService
import com.example.kvartirkatestapplication.models.CitiesFlatsModel
import com.example.kvartirkatestapplication.models.CityModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainRepository(private val apiService: ApiService) {

    private val compositeDisposable = CompositeDisposable()
    private val tag = "MainRepository"
    private val cityFlats = MutableLiveData<List<CitiesFlatsModel.Flat>>()
    private val errorMessage = MutableLiveData<String>()
    private val cityName = MutableLiveData<String>()


    fun getCityByGeoPoint() {
        compositeDisposable.add(
            apiService.getCityByGeoPoint(Values.lat.toString(), Values.lon.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ setCityIdValue(it) }, { onError(it) }, { Log.d(tag, "onComplite") })
        )
    }

    private fun setCityIdValue(it: CityModel) {
        if (it.cities.isNotEmpty()) {
            Values.city_id = it.cities[0].id
            getFlatsByGeoPoint()

            Log.d(tag, "response  = ${it.cities[0].name}")
            Log.d(tag, "response  = ${it.cities[0].id}")
            cityName.value = it.cities[0].name
        }
    }


    fun getFlatsByGeoPoint() {
        compositeDisposable.add(
            apiService.getFlatsByGeoPoint(
                Values.city_id,
                Values.lat,
                Values.lon,
                Values.screenWidth,
                Values.screenHeight
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { addFlatsInList(it) },
                    { getFlatsByGeoPointError(it) },
                    { Log.d(tag, "onComplite") })
        )
    }

    private fun getFlatsByGeoPointError(it: Throwable?) {
        Log.d(tag, "getFlatsByGeoPointError  = ${it?.localizedMessage}")
        errorMessage.value = it?.message
    }

    private fun addFlatsInList(it: CitiesFlatsModel?) {
        cityFlats.value = it?.flats
        Log.d(tag, "flats size = ${it?.flats?.size}")
        for (item in it?.flats!!) {
            Log.d(tag, "flat address = ${item.address}")
            Log.d(tag, "flat photo = ${item.photo_default.url}")
            Log.d(tag, "flat url = ${item.url}")
            Log.d(tag, "flat id = ${item.id}")
        }

    }

    private fun onError(t: Throwable) {
        Log.d(tag, "response error = ${t.message}")

    }

    fun getCityFlats(): MutableLiveData<List<CitiesFlatsModel.Flat>> {
        return cityFlats
    }
    fun getErrorMessage():MutableLiveData<String>{
        return errorMessage
    }
    fun getCityName():MutableLiveData<String>{
        return cityName
    }

    fun onCleared() {
        compositeDisposable.clear()
    }
}