package com.example.kvartirkatestapplication.di

import android.app.Application
import com.example.kvartirkatestapplication.di.DependencyModule.currentFlatModule
import com.example.kvartirkatestapplication.di.DependencyModule.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class KvartirkaApp :Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@KvartirkaApp)
            modules(listOf(mainModule,currentFlatModule))
        }
    }
}