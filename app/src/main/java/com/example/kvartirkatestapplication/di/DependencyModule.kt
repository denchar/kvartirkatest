package com.example.kvartirkatestapplication.di

import com.example.kvartirkatestapplication.data.MainRepository
import com.example.kvartirkatestapplication.data.retrofit.ApiClient
import com.example.kvartirkatestapplication.data.retrofit.ApiService
import com.example.kvartirkatestapplication.ui.currentflat.CurrentFlatViewModel
import com.example.kvartirkatestapplication.ui.currentflat.PhotoAdapter
import com.example.kvartirkatestapplication.ui.flats.FlatsAdapter
import com.example.kvartirkatestapplication.ui.flats.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object DependencyModule {
    val mainModule = module {
        viewModel {
            MainViewModel(
                get(),
                get()
            )
        }
        single { MainRepository(get()) }
        single { ApiClient.getApiRetrofitClient()?.create(ApiService::class.java) }
        single { FlatsAdapter() }
    }
    val currentFlatModule = module {
        viewModel { CurrentFlatViewModel(get()) }
        single { PhotoAdapter() }
    }
}