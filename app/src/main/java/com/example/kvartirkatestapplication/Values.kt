package com.example.kvartirkatestapplication

class Values {
    companion object {
        const val latCapital = 55.755401
        const val lonCapital = 37.617820
        const val idCapital = 643

        var city_id = idCapital
        var lat: Double = 0.0
        var lon: Double = 0.0

        var flat_id: Int = 0
        var screenWidth = 936
        var screenHeight = 624

    }
}