package com.example.kvartirkatestapplication.ui.flats

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Point
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.kvartirkatestapplication.CurrentFlatCache
import com.example.kvartirkatestapplication.R
import com.example.kvartirkatestapplication.Values
import com.example.kvartirkatestapplication.databinding.ActivityMainBinding
import com.example.kvartirkatestapplication.models.CitiesFlatsModel
import com.example.kvartirkatestapplication.ui.currentflat.CurrentFlatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.math.abs


class MainActivity : AppCompatActivity(), FlatsAdapter.ViewHolderClickListener {

    private val mainViewModel: MainViewModel by viewModel()
    private val flatsAdapter: FlatsAdapter by inject()
    private lateinit var activityMainBinding: ActivityMainBinding

    lateinit var locationListener: LocationListener
    lateinit var locationManager: LocationManager
    private val PERMISSIONS_REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        activityMainBinding.viewModel = mainViewModel
        activityMainBinding.executePendingBindings()

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            checkPermission(
                Manifest.permission.ACCESS_FINE_LOCATION,
                PERMISSIONS_REQUEST_CODE
            )
            return
        } else {
            onPermissionGranted()
        }
        setBinding()
    }

    private fun setBinding() {
        setDisplayWidthHeight()
        flatsAdapter.setListener(this)
        flats_recycler_view.adapter = flatsAdapter
        mainViewModel.getCityFlats().observe(this, Observer<List<CitiesFlatsModel.Flat>> { list ->
            flatsAdapter.addListInData(list)
            mainViewModel.isLoading.set(false)
        })
        mainViewModel.getErrorMessage().observe(this, Observer<String> {
            mainViewModel.isLoading.set(false)
        })
        mainViewModel.getCityName().observe(this, Observer<String> {
            mainViewModel.cityName.set(it)
        })

        swipe_refresh_layout.setOnRefreshListener {
            mainViewModel.getFlatsByGeoPoint()
            swipe_refresh_layout.isRefreshing = false
        }

    }

    private fun checkPermission(
        permission: String,
        requestCode: Int
    ) {
        if (ContextCompat.checkSelfPermission(
                this,
                permission
            ) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)

        } else {
            Log.d(this.localClassName, "Permission already granted")
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)
                ) {
                    Log.d(this.localClassName, " permission ok")
                    onPermissionGranted()
                    setBinding()

                } else {
                    Log.d(this.localClassName, " permission cancelled")
                }
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun setLocation(location: Location?) {
        if (location == null) {
            Log.d(this.localClassName, "location is empty")
            if (Values.lat == Values.latCapital && Values.lon == Values.lonCapital) {
                Log.d(this.localClassName, "location is Capital Location")
            } else {
                Values.lat = Values.latCapital
                Values.lon = Values.lonCapital
                mainViewModel.getCityByGeoPoint()
            }
        } else {
            Log.d(
                this.localClassName,
                "lat = ${location.latitude}, lon = ${location.longitude}," +
                        " time = ${Date(location.time)}"
            )

            val minDistanceLat = abs(Values.lat - location.latitude)
            val minDistanceLon = abs(Values.lon - location.longitude)
            if (minDistanceLat > 0.001 && minDistanceLon > 0.001) {
                Values.lat = location.latitude
                Values.lon = location.longitude
                mainViewModel.getCityByGeoPoint()
            }

        }
    }
    private fun setDisplayWidthHeight(){
        val display = windowManager.defaultDisplay
        val point = Point()
        display.getSize(point)
        Values.screenWidth =point.x
        Values.screenHeight = point.y
    }

    private fun onPermissionGranted() {
        Log.d(this.localClassName, "onPermissionGranted")
        locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location?) {
                setLocation(location)
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

            }

            override fun onProviderEnabled(provider: String?) {

            }


            override fun onProviderDisabled(provider: String?) {
                checkPermission(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    PERMISSIONS_REQUEST_CODE
                )
                setLocation(locationManager.getLastKnownLocation(provider))

            }
        }
        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            1000 * 10, 10f, locationListener
        )

        setLocation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER))
    }

    override fun onViewHolderClick(item: CitiesFlatsModel.Flat) {
        Log.d(this.localClassName, "current flat id = ${item.id}")
        CurrentFlatCache.flatCache = item
        CurrentFlatActivity.toCurrentFlatActivity(this)
    }
}