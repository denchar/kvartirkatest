package com.example.kvartirkatestapplication.ui.flats

import android.app.Application
import android.renderscript.Sampler
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.kvartirkatestapplication.Values
import com.example.kvartirkatestapplication.data.MainRepository
import com.example.kvartirkatestapplication.models.CitiesFlatsModel

class MainViewModel(application: Application, private val repository: MainRepository) :
    AndroidViewModel(application) {

    var isLoading = ObservableField<Boolean>(false)
    var cityName = ObservableField<String>("")

    fun getCityByGeoPoint() {
        isLoading.set(true)
        repository.getCityByGeoPoint()
    }

    fun getFlatsByGeoPoint() {
        isLoading.set(true)
        repository.getFlatsByGeoPoint()
    }

    fun getCityFlats(): MutableLiveData<List<CitiesFlatsModel.Flat>> {
        return repository.getCityFlats()
    }

    fun getErrorMessage(): MutableLiveData<String> {
        return repository.getErrorMessage()
    }
    fun getCityName(): MutableLiveData<String> {
        return repository.getCityName()
    }

    override fun onCleared() {
        super.onCleared()
        repository.onCleared()

    }

}