package com.example.kvartirkatestapplication.ui.flats

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kvartirkatestapplication.CurrentFlatCache
import com.example.kvartirkatestapplication.R
import com.example.kvartirkatestapplication.models.CitiesFlatsModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_flat.view.*

class FlatsAdapter : RecyclerView.Adapter<FlatsAdapter.ViewHolder>() {

    private var data = ArrayList<CitiesFlatsModel.Flat>()
    private lateinit var viewHolderClickListener: ViewHolderClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_flat, parent, false),
            viewHolderClickListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    fun setListener(viewHolderClickListener: ViewHolderClickListener) {
        this.viewHolderClickListener = viewHolderClickListener
    }

    fun addListInData(list: List<CitiesFlatsModel.Flat>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    interface ViewHolderClickListener {
        fun onViewHolderClick(item: CitiesFlatsModel.Flat)
    }

    class ViewHolder(view: View, viewHolderClickListener: ViewHolderClickListener) :
        RecyclerView.ViewHolder(view) {
        var poster: ImageView = view.poster_image_view
        var address: TextView = view.address_text_view
        var price: TextView = view.price_text_view
        lateinit var flatModel: CitiesFlatsModel.Flat

        init {
            itemView.setOnClickListener {
                viewHolderClickListener.onViewHolderClick(flatModel)
            }
        }

        fun bind(flatModel: CitiesFlatsModel.Flat) {
            this.flatModel = flatModel
            this.address.text = flatModel.address
            this.price.text = ("Цена: день ${flatModel.prices.day}," +
                    " ночь ${flatModel.prices.night}, " +
                    "час ${flatModel.prices.hour}")
            val url = flatModel.photo_default.url
            Picasso.get().load(url).into(this.poster)
        }
    }
}