package com.example.kvartirkatestapplication.ui.currentflat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.kvartirkatestapplication.R
import com.example.kvartirkatestapplication.models.CitiesFlatsModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_photo.view.*


class PhotoAdapter : RecyclerView.Adapter<PhotoAdapter.ViewHolder>() {

    private var data = ArrayList<CitiesFlatsModel.Photos>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    fun addListInData(list: List<CitiesFlatsModel.Photos>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var photo: ImageView = view.photo_view
        fun bind(item: CitiesFlatsModel.Photos) {
            Picasso.get().load(item.url).into(photo)
        }
    }
}