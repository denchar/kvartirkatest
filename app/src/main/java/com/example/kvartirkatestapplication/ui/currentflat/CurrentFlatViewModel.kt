package com.example.kvartirkatestapplication.ui.currentflat

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.example.kvartirkatestapplication.CurrentFlatCache

class CurrentFlatViewModel(application: Application) : AndroidViewModel(application) {

    val address = ObservableField<String>()
    val type = ObservableField<String>()
    val roomsCount = ObservableField<String>()
    val price = ObservableField<String>()
    val description = ObservableField<String>()
    val isLoading = ObservableField<Boolean>(false)

    init {
        binding()
    }

    private fun binding() {
        //  !!!!  prices": {"day": 1200, "night": 0, "hour": 0}
        address.set("Адрес: " + CurrentFlatCache.flatCache.address)
        type.set("Тип жилья: " + CurrentFlatCache.flatCache.title)
        roomsCount.set("Комнат: " + CurrentFlatCache.flatCache.rooms)
        price.set(
            "Цена: день ${CurrentFlatCache.flatCache.prices.day}," +
                    " ночь ${CurrentFlatCache.flatCache.prices.night}, " +
                    "час ${CurrentFlatCache.flatCache.prices.hour}"
        )
        description.set(CurrentFlatCache.flatCache.description)
        isLoading.set(true)
    }
}