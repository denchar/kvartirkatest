package com.example.kvartirkatestapplication.ui.currentflat

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.kvartirkatestapplication.CurrentFlatCache
import com.example.kvartirkatestapplication.R
import com.example.kvartirkatestapplication.databinding.ActivityCurrentFlatBinding
import kotlinx.android.synthetic.main.activity_current_flat.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class CurrentFlatActivity : AppCompatActivity() {

    lateinit var activityCurrentFlatBinding: ActivityCurrentFlatBinding
    private val currentFlatViewModel: CurrentFlatViewModel by viewModel()
    private val photoAdapter: PhotoAdapter by inject()

    companion object {
        fun toCurrentFlatActivity(context: Context) {
            context.startActivity(Intent(context, CurrentFlatActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_current_flat)


        activityCurrentFlatBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_current_flat)
        activityCurrentFlatBinding.viewModel = currentFlatViewModel
        activityCurrentFlatBinding.executePendingBindings()

        photoAdapter.addListInData(CurrentFlatCache.flatCache.photos)
        photo_recycler_view.adapter = photoAdapter
        currentFlatViewModel.isLoading.set(false)
    }

}
