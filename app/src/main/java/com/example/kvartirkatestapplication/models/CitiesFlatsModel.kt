package com.example.kvartirkatestapplication.models

data class CitiesFlatsModel(val flats: List<Flat>) {
    class Flat(
        val id: Int,
        val city_id: Int,
        val building_type: String,
        val metro: String,
        val rooms: String,
        val address: String,
        val url: String,
        val coordinates: Coordinates,
        val description: String,
        val prices: Prices,
        val photo_default: PhotoDefault,
        val contacts: Contacts,
        val photos: List<Photos>,
        val title: String,
        val description_full: String,
        val badges: Badges
    )


    class Coordinates(val lon: Double, val lat: Double)
    class Prices(val day: String, val night: String, val hour: String)
    class PhotoDefault(val url: String, val verified: Boolean, val width: Int, val height: Int)
    class CurrentPhones(val phone: String, val normalized: String)
    class Contacts(
        val id: Int,
        val flats_count: Int,
        val name: String?,
        val skype: String?,
        val vk_profile: String,
        val send_booking_request_allowed: Boolean,
        val show_prepayment_warning: Boolean,
        val phones: List<CurrentPhones>
    )

    class Photos(val url: String, val verified: Boolean)
    class Badges(val owner_confirmed: Boolean, val more_than_year: Boolean, val payed: Int)


}
