package com.example.kvartirkatestapplication.models

data class CityModel(val meta: Meta, val cities: List<City>, val version: Double) {

    class Meta(val total: Int, val filters: Filter)

    class Filter(val place: Place)

    class Place(val lat: Double, val lng: Double)

    class City(
        val id: Int,
        val name: String,
        val name_genitive: String,
        val name_prep: String,
        val coordinates: Coordinates,
        val country: Country,
        val region: Region,
        val important: Boolean
    )

    class Region(val id: Int, val name: String)

    class Coordinates(val lat: Double, val lon: Double)

    class Country(val id: Int, val name: String)
}